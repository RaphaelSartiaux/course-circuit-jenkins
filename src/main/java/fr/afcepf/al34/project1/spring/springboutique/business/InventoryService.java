package fr.afcepf.al34.project1.spring.springboutique.business;

import java.util.List;

import fr.afcepf.al34.project1.spring.springboutique.entity.Inventory;
import fr.afcepf.al34.project1.spring.springboutique.entity.Product;
import fr.afcepf.al34.project1.spring.springboutique.entity.Type;


public interface InventoryService {

	Inventory saveInBase(Inventory inventory);
	//	Inventory findWithProduct(Product product);
	List<Type> getAllTypes();
	Type saveInBase(Type type);
//	void increaseMinStock (int minStock);
//	void decreaseMinStock (int minStock);
	List<String> getAllColors();
	List<String> getAllSizes();
	Inventory getTotalMinStock(Integer idInventory);
	Inventory getTotalQuantity(Integer idInventory);

}
