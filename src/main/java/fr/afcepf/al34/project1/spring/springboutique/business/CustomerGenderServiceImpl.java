package fr.afcepf.al34.project1.spring.springboutique.business;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.afcepf.al34.project1.spring.springboutique.dao.CustomerGenderDao;
import fr.afcepf.al34.project1.spring.springboutique.entity.CustomerGender;

@Service
@Transactional
public class CustomerGenderServiceImpl implements CustomerGenderService {
	@Autowired
	CustomerGenderDao customerGenderDao;


	@Override
	public CustomerGender saveInBase(CustomerGender cg) {
		return customerGenderDao.save(cg);
	}


	@Override
	public List<CustomerGender> getAllCustomerGenders() {
		return (List<CustomerGender>)customerGenderDao.findAll();
	}
}
