package fr.afcepf.al34.project1.spring.springboutique.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter @NoArgsConstructor @ToString()
public class StockDto {

	private Integer idProduit;
	public int quantity;
	public int quantityMin;
	public double price;
	public double cost;


	public StockDto(Integer idProduit, int quantity, int quantityMin, float price, float cost) {
		super();
		this.idProduit = idProduit;
		this.quantity = quantity;
		this.quantityMin = quantityMin;
		this.price = price;
		this.cost = cost;
	}

	


	
}
