package fr.afcepf.al34.project1.spring.springboutique.service;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.afcepf.al34.project1.spring.springboutique.dto.StockDto;

@Service
public class StockServiceDelegate implements IStockService{

	ObjectMapper jsonMapper = new ObjectMapper();
	private RestTemplate restTemplate = new RestTemplate();
	
	@Value("${stock-api.base-url}") //pour récupérer 
	private String baseUrl;

	@Override
	public StockDto getStockByID(Integer Id, Integer IdClient) {
		String url = baseUrl + "/" +Id+","+IdClient;
		return restTemplate.getForObject(url,StockDto.class);
	}

	@Override
	public StockDto getStockByIdProduit(Integer IdProduit) {
		String url= baseUrl+"/"+IdProduit;
		return restTemplate.getForObject(url, StockDto.class, IdProduit);
	}

	@Override
	public List<StockDto> getAllStock() {
		StockDto[] tabStock = null;
		String url= baseUrl;
		tabStock = restTemplate.getForObject(url, StockDto[].class);
		return Arrays.asList(tabStock);
	}

	@Override
	public void postStock(StockDto Stock) {
		String url = baseUrl+"/";
		 restTemplate.postForObject(url,Stock,StockDto.class);
	}

	@Override
	public void DeleteStock(StockDto Stock) {
		String url = baseUrl;
	restTemplate.delete(url, Stock);
	}

	@Override
	public void changeStock(StockDto Stock) {
		String url = baseUrl+"/change/"+Stock.getIdProduit()+"/";
		try{
			restTemplate.postForObject(url,Stock,StockDto.class);
		}
		catch(Exception e) {
			System.out.println("");
		}
	}

	
	
}
