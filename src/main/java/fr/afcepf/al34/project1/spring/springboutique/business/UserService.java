package fr.afcepf.al34.project1.spring.springboutique.business;


import java.util.List;

import fr.afcepf.al34.project1.spring.springboutique.entity.Gender;
import fr.afcepf.al34.project1.spring.springboutique.entity.User;

public interface UserService {

	User saveInBase(User user);
	Gender saveInBase(Gender gender);
	List<Gender> getAllGenders();
}
