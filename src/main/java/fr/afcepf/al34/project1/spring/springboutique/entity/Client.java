package fr.afcepf.al34.project1.spring.springboutique.entity;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor
@Entity
@DiscriminatorValue("Client")
public class Client extends User implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
    private String email;

    @Column(name = "phone_number")
    private String phoneNumber;

    @OneToOne(mappedBy = "client")
    private Cart cart;

    @OneToMany
    private List<BankCard> bankCard = new ArrayList<BankCard> ();

    @Column(name = "delivery_addresses")
    @OneToMany
    private List<DeliveryAddress> deliveryAddresses = new ArrayList<DeliveryAddress> ();

    @OneToMany(mappedBy = "client")
    private List<Rating> ratings = new ArrayList<Rating> ();
    
    public Client(String firstName, String lastName, String email, String phoneNumber,
    		Credentials credentials, Gender gender) {
    	this.setFirstName(firstName);
    	this.setLastName(lastName);
    	this.email = email;
    	this.phoneNumber = phoneNumber;
    	this.setCredentials(credentials);
    	this.setGender(gender);
    }

}
